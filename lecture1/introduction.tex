\documentclass{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[BBGW 6.3.5]{Globale Biogeochemische Stoffkreisl\"aufe (BBGW 6.3.5)}
\author{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{11. 04. 2019}
%\titlegraphic{\includegraphics[scale=0.3]{../MPG_Minerva_EPS.eps}}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Modulbeschreibung}
\begin{itemize}
\item Modulcode: BBGW6.3.5
\item Voraussetzung f\"ur die Zulassung zum Modul: Keine
\item Art des Moduls: Wahlplichtmodul
\item H\"aufigkeit des Angebots: jedes 2. Semester (ab Sommersemester)
\item Leistungspunkte: 3 LP
\item Unterrichtsspache: Englisch/Deutsch
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Modulbeschreibung}
Arbeitsaufwand in:
\begin{itemize}
\item Pr\"asenzstunden: 30 h
\item Selbstudium: 60 h
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Lern- und Qualifikationsziele}
\centering
\emph{Die Studierenden vertiefen ihre Kenntnisse \"uber die globalen Stoff- und Energiekreisl\"aufe. Sie erfassenden wachsenden Einfluss des Menschen auf diese Kreisl\"aufe und setzen sich kritisch mit dem Zusammenhang zum globalen Wandel auseinander. Die Abfassung einer themenorientierten schriftlichen Facharbeit wird eing\"ubt.}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Termine}
\small
\begin{itemize}
\item 11.04. Intro to global biogeochemical cycles
\item 18.04. The global C cycle I (Atmosphere and terrestrial biosphere)
\item 25.04. TBA % Global photosynthesis (replacement, Miguel?)
\item 02.05. The global C cycle II (redox chemistry, methane and oceans)
\item 09.05. Modelling Biogeochemical cycles I
\item 16.05. Modelling Biogeochemical cycles II % replacement or practical exercise
\item 23.05. The global N cycle
\item 30.05. Christi Himmelfahrt (Feiertag)
\item 06.06. The global P cycle
\item 13.06. The global S cycle
\item 20.06. Fronleichnam (Feiertag)
\item 27.06. Oxygen, water, and long-term Earth's climate history
\item 04.07. Earth's energy balance and entropy production
\item ??.07. Exam
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Website}
\centering
\url{https://gitlab.com/sierracrl/gbc}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Assigned reading}
\centering
\includegraphics[scale=0.7]{Jacobson} \\ \vspace{1em}
Chapter 1: Biogeochemical cycles as fundamental constructs for studying Earth system science
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth as a system}
\begin{itemize}
\item Earth is a closed system with regard to matter and open with respect to energy
\item Matter and energy follow conservation laws
\item Mass redistributes naturally among different components of the Earth
\item Mass distribution is mostly driven by solar radiation and dynamics of Earth's interior
\item Humans now accelerate the rates at which matter moves
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth is both an open and a closed system}
\centering
\includegraphics[scale=0.6]{openClosedSystem.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Matter and energy follow conservation laws}
\begin{itemize}
\item Energy cannot be created or destroyed, it can only be transformed from one form into another.
\item Mass cannot be created or destroyed. The change of mass $M$ over time is the result of inputs minus outputs of mass. 

\begin{equation}
\frac{dM}{dt} = Inputs - Outputs
\end{equation}
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Mass and energy redistribution}
\centering
\includegraphics[scale=0.3]{massEnergyDistribution}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Dynamics of Earth's interior}
\includegraphics[scale=0.34]{erde1}
\includegraphics[scale=0.35]{erde2} \\
\includegraphics[scale=0.3]{plateTectonics}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Solar radiation fuels Earth's biosphere}
\centering
\includegraphics[scale=0.23]{global_biosphere} \\ \tiny \vspace{1cm} {\it \textcopyright SeaWiFS project, NASA}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Solar radiation drives the climate system}
\centering
\includegraphics[scale=0.4]{hadley_cells}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Biogeochemical cycles and the `spheres'}
\begin{itemize}
\item Carbon, Nitrogen, Phosphorus and Sulfur are the main biogeochemical cycles. They are part of all life and actively interact with other components of the Earth system
\item The Earth system can also be compartmentalized in `geospheres' to make the study of the Earth system more manageable
\item These are : atmosphere, hydrosphere, lithosphere, pedosphere, and the biosphere
\item They are more or less well defined, but in many cases it is hard to define their boundaries
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Essential elements for life}
\centering
\includegraphics[scale=0.5]{elements.png}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth's biosphere}
\begin{multicols}{2}
\centering
\includegraphics[scale=0.9]{home-earth-spheres} \\
\columnbreak
 \tiny
{\it The term ``biosphere" originated with the geologist {\bf Eduard Suess} in 1875, who defined it as ``the place on earth's surface where life dwells". {\bf Vladimir I. Vernadsky} first defined the biosphere in a form resembling its current ecological usage in his long-overlooked book of the same title, originally published in 1926. It is Vernadsky's work that redefined ecology as the science of the biosphere and placed the biosphere concept in its current central position in earth systems science. \\ \vspace{0.5cm} Ellis (2009, The Encyclopedia of Earth)}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The terrestrial biosphere within the Earth system}
%\centering
{\bf Definition}: \\
%\includegraphics[scale=0.4, trim=20cm 15cm 0cm 0cm, clip]{Resources/GPP_Beer} \\ \small
The terrestrial biosphere is the sum of all terrestrial {\it ecosystems} on Earth
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Human modification of Earth}
\begin{itemize}
\item Humans are accelerating the rate at which matter moves around
\item They are modifying the radiation budget of Earth
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Human modification of Earth}
\centering
\includegraphics[scale=0.21]{MaunaLoa} 
\includegraphics[scale=0.23]{GlobalTemp} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Human modification of the biosphere}
\includegraphics[scale=0.1]{Vitousek} \\ \vspace{1cm} \tiny
Vitousek et al. (1997, Science 277, 494)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Human modification of the biosphere}
\includegraphics[scale=0.4]{global_changes1} \\ \vspace{1cm} \tiny
Steffen et al. (2005, Global Change and the Earth System)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Human modification of the biosphere}
\includegraphics[scale=0.4]{global_changes} \\ \vspace{1cm} \tiny
Steffen et al. (2005, Global Change and the Earth System)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Human modification of the terrestrial biosphere}
\includegraphics[scale=0.2]{biosphere_change}  \vspace{1cm} \tiny
Ellis (2011, Phil Trans A, 369: 1010)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Atmospheric CO$_2$ over the past 42,000 years}
\includegraphics[scale=0.9]{Historical_CO2} \\ \vspace{1cm}
\tiny Anatomically modern humans arose in Africa about 200,000 years ago. \\
Reached behavioral modernity about 50,000 years ago. \\
The Neolithic Revolution began 12,000 years ago. 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Modification of the N cycle}
\includegraphics[scale=0.65]{Ncycle1} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%\frame{
%\centering
%\frametitle{Modification of the N cycle}
%\includegraphics[scale=0.65]{Ncycle2} 
%}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Modification of the P cycle}
\includegraphics[scale=0.65]{Phistoric} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Modification of the P cycle}
\includegraphics[scale=0.5]{Ppeak} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Human impacts on the Earth system}
Human impacts on the Earth system
\begin{itemize}
\item are approaching or exceeding in {\it magnitude} some of the great forces of nature
\item operate on much faster time scales than {\it rates} of natural variability, often by an order of magnitude or more
\item taken together in terms of extent, magnitude, rate and simultaneity, have produced a {\it no-analogue} state in the dynamics and functioning of the Earth system. 
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\centering
\frametitle{Possible trajectories of ecosystems and global change processes}
\includegraphics[scale=1]{future_trajectories} 
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{History of biogeochemistry}
\begin{itemize}
\item 1788 -- James Hutton: Viewed the Earth as a superorganism that can be studied as in physiology
\item 1896 -- Svante Arrhenius: First described the possible role of carbon dioxide on the temperature of the Earth
\item 1875 -- Eduard Suess: Coined the term biosphere
\item 1926 -- Vladimir Vernadsky -- Further developed the concept of biosphere
\item 1974 -- Lovelock \& Margulis -- Concept of Gaia as a self regulatory system
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth outside thermodynamic equilibrium}
\centering
\includegraphics[scale=0.3]{thermodinamicDisequilibrium}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Earth outside thermodynamic equilibrium}
\centering
\includegraphics[scale=0.3]{thermodynamicEarth}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Compartmental approach to understand cycles}
\begin{itemize}
\item Why the cycle approach
\item How we do conceptualize these cycles in a more manageable form?
\item Law of mass balance
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The global C cycle: IPCC}
\centering
\includegraphics[scale=0.45]{cCycleIPCC}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The water cycle}
\centering
\includegraphics[scale=0.6]{waterCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Compartmental theory}
Definitions:
\begin{itemize}
\item Compartment/reservoir: an amount of material defined by certain physical, chemical, or biological characteristics and is kinetically homogeneous. We characterize compartments by their mass.
\item Flux: amount of material transferred from one compartment to another per unit of time, i.e. [mass time$^{-1}$] 
\item Rate: relative speed of change of the mass of a reservoir in units of inverse time, i.e. [time$^{-1}$]
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Compartmental structure of models}
\centering
\includegraphics[scale=0.5]{Pools}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary -- Take home messages}
\begin{itemize}
\item Earth is a closed system with regard to matter and open with respect to energy
\item Matter and energy follow conservation laws
\item Mass redistributes naturally among different components of the Earth
%\item Mass distribution is mostly driven by solar radiation and dynamics of Earth's interior
%\item Humans now accelerate the rates at which matter moves
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary -- Take home messages}
\begin{itemize}
\item Carbon, Nitrogen, Phosphorus and Sulfur are the main biogeochemical cycles. They are part of all life and actively interact with other components of the Earth system
\item The Earth system can also be compartmentalized in `geospheres' to make the study of the Earth system more manageable
%\item These are : atmosphere, hydrosphere, lithosphere, pedosphere, and the biosphere
%\item They are more or less well defined, but in many cases it is hard to define their boundaries
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Summary -- Take home messages}
\begin{itemize}
\item We will take a compartmental approach to study biogeochemical cycles.
\item Main concepts are: compartment, flux, rate.
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

