\documentclass[aspectratio=169]{beamer}

 \usepackage{beamerthemesplit} %// Activate for custom appearance
 \usepackage{multicol}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{graphicx}
\usepackage{chemformula}

\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\deriv}[1]{\frac{d}{d#1}}
\newcommand{\intl}{\int\limits}

%\usetheme{Luebeck}
\usetheme[compress]{MPIM}
 \usecolortheme{orchid}

\title[The global C cycle]{The Global Carbon Cycle (Part II): Oceans and Methane}
\author[Sierra, C.A.]{Carlos A. Sierra}
\institute{Max Planck Institute for Biogeochemistry}
\date{May 2, 2019}

\usefonttheme{serif}

\begin{document}

%%%%%%%%%%%%%%
\frame{\titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Outline}
\begin{itemize}
\item Carbon cycle in the oceans: DOC and DIC
\item Methane budget
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Assigned reading}
\centering
\includegraphics[scale=0.25]{reading1}
\includegraphics[scale=0.15]{reading2}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{The global C cycle}
\centering
\includegraphics[scale=0.4]{../lecture2/CCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Uptake of C from natural sinks}
\centering
\includegraphics[scale=0.3]{../lecture2/gcp1}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Strength of natural sinks}
\centering
\includegraphics[scale=0.32]{../lecture2/gcp2}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Marine carbon reservoirs}
\begin{itemize}
\item {\bf DIC}: Dissolved Inorganic C ($< 1 \mu$m) \; 38,100 Pg C
\item {\bf DOC}: Dissolved Organic C ($< 0.2 \mu$m) \; 662 Pg C
\item {\bf POC}: Particulate Organic C ($> 0.2 \mu$m) \; 25 Pg C
\item {\bf BC}: Black C ($< 0.2 \mu$m) \; $> 14$ Pg C
\item {\bf SOC}: Sedimentary Organic C ($0-1$ m) \; 150 Pg C
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Mechanisms for carbon ocean uptake and recycling}
\begin{itemize}
\item Abiotic inorganic cycling
\item Biotic carbon uptake and cycling
\end{itemize}

\centering
\includegraphics[scale=0.25]{bioticAbiotic}

}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Abiotic inorganic carbon cycling}
\centering
\includegraphics[scale=0.5]{airseaExchange} \\
\vspace{2em}
Sea-air gas exchange is mainly determined by wind speed and CO$_2$ solubility.
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Solubility of CO$_2$ is temperature dependent}
\centering
\includegraphics[scale=0.6]{solubility}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Equilibrium reactions in the ocean}
\centering
\ch{CO2} hydrates to \ch{H2CO3} \\
\vspace{2em}
\ch{CO2 + H2O <-> H2CO3} \\
\ch{H2CO3} (0.5\%) \ch{<-> H+ + HCO3-} (89\%) \\
\ch{HCO3- <-> H+ + CO3^2-} (10.5\%)
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Equilibrium reactions in the ocean}
\centering
\includegraphics[scale=0.4]{pHRx}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Winds are a major driver of DIC transport in the surface ocean}
\centering
\includegraphics[scale=0.4]{winds}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Wind-driven transport}
\centering
\includegraphics[scale=0.4]{latitudinalTransport}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Surface currents and DIC transport}
\centering
\includegraphics[scale=0.5]{surfaceCurrents}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Thermohaline circulation (conveyor belt)}
\centering
\includegraphics[scale=0.5]{thermohaline}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Radiocarbon in the deep ocean}
\centering
\includegraphics[scale=0.6]{deepRadiocarbon}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Ocean acidification}
\centering
\includegraphics[scale=0.5]{acidification}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Biotic carbon cycling}
\centering
\includegraphics[scale=0.85]{bioticC}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Marine organic matter}
\centering
\includegraphics[scale=0.7]{mom}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Cycling og marine organic matter}
\centering
\includegraphics[scale=0.5]{momCycle}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DOC concentrations globally}
\centering
\vspace{-2em}
\includegraphics[scale=0.4]{docStructure}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DOC is much older than DIC}
\centering
\includegraphics[scale=0.38]{docRadiocarbon}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{DOC concentrations and carbon age}
\centering
\includegraphics[scale=0.5]{docAge}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Why is marine DOC so old?}
\centering
\includegraphics[scale=0.5]{whyDocAge}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Methane CH$_4$}
\centering
\includegraphics[scale=1]{ch4IPCC}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Why is methane important?}
\begin{itemize}
\item After carbon dioxide (CO$_2$), methane (CH$_4$) is the second most important greenhouse gas contributing to human-induced climate change. 
\item For a time horizon of 100 years, CH$_4$ has a Global Warming Potential 28 times larger than CO$_2$. 
\item Methane is responsible for 20\% of the global warming produced by all greenhouse gases so far.
\item The concentration of \ch{CH4} in the atmosphere is 150\% above pre-industrial levels (cf. 1750).
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Why is methane important?}
\begin{itemize}
\item The atmospheric life time of CH$_4$ is $9 \pm 2$ years, making it a good target for climate change mitigation
\item Methane also contributes to tropospheric production of ozone, a pollutant that harms human health and ecosystems. 
\item Methane also leads to production of water vapor in the stratosphere by chemical reactions, enhancing global warming.
\item It is an important source of energy for humans!
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Methane increase since pre-industrial levels}
\centering
\includegraphics[scale=0.8]{ch4Increase}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Methane in the Holocene}
\centering
\includegraphics[scale=1]{holoceneCH4}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Slowdown and increase in atmospheric CH$_4$}
\centering
\includegraphics[scale=0.35]{ch4Growth1}
\includegraphics[scale=0.35]{ch4Growth2}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Methane and climate change scenarios}
\centering
\includegraphics[scale=0.45]{ch4Scenarios}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Methane sources and sinks}
\centering
\includegraphics[scale=0.5]{gcpCH4}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{How is methane produced?}
\begin{itemize}
\item Abiotic: Generation of natural gas - `cooking' organic rich sediments at high temperatures in a reduced oxygen environment
\item Biotic: Fermentation, decomposition of organic matter in anoxic environments (guts of animals, sediments, wetlands, landfills)
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Biotic methane production and consumption}
\begin{multicols}{2}
{\bf Methanogenesis}

\begin{itemize}
\item Archea, strict anaerobes
\item \ch{CO2} reduction \\
        \ch{CO2 + 4 H2 -> CH4 + 2 H2O} 
\item Acetate splitting (fermentation) \\
        \ch{CH3COOH -> CO2 + CH4}
\end{itemize}

\columnbreak
{\bf Methanotrophy}
\begin{itemize}
\item Aerobic methane oxidation
\item Anaerobic methane oxidation \\
        \ch{CH4 + SO4^2- -> HS- + HCO3^- + H2O} \\
        \ch{CH4 + 4 NO3- -> CO2 + 4 NO2- + 2 H2O}
\end{itemize}
\end{multicols}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Fate of methane after production}
\centering
\includegraphics[scale=0.4]{wetlandCH4}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Fate of methane after release}
\centering
\includegraphics[scale=0.37]{ch4Sinks}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Take-home messages: Ocean carbon cycle}
\begin{itemize}
\item Carbon cycling in the oceans is controlled by abiotic and biotic processes
\item Air-sea \ch{CO2} exchange is an important physical process responsible for the large reservoir of dissolved inorganic carbon (DIC)
\item Winds and currents mix DIC in the world's oceans
\item Additional \ch{CO2} in the atmosphere leads to ocean acidification
\item Dissolved organic carbon (DOC) is linked to biological processes and complex food-webs
\item DOC is very old!
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\frame{
\frametitle{Take-home messages: Methane}
\begin{itemize}
\item Atmospheric methane has increased dramatically since the industrial revolution
\item Fossil fuel combustion, agriculture, and wetlands are the major methane sources to the atmosphere
\item Methane in wetlands is mostly produce under anaerobic conditions
\item In the atmosphere, methane is degraded by OH radicals and tropospheric chlorine
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}


