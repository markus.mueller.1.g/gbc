# Globale Biogeochemische Stoffkreisläufe (BBGW 6.3.5)
Here you find all course material. In the lecture# folders, you find all slides and figures for all lectures. 
In the reading folder, you find all assigned reading material for the course.

## Termine 
- 11.04. Intro to global biogeochemical cycles
- 18.04. The global C cycle I (Atmosphere and terrestrial biosphere)
- 25.04. The water cycles and stable isotopes (Prof. Trumbore) 
- 02.05. The global C cycle II (methane and oceans)
- 09.05. Modelling Biogeochemical cycles I
- 16.05. Modelling Biogeochemical cycles II 
- 23.05. The global N cycle
- 30.05. Christi Himmelfahrt (Feiertag)
- 06.06. The global P cycle
- 13.06. The global S cycle
- 20.06. Fronleichnam (Feiertag)
- 27.06. Oxygen, water, and long-term Earth's climate history
- 04.07. Earth's energy balance and entropy production
- 11.07. Exam
